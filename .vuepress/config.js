module.exports = {
  title: 'Storefront\'s technical documentation',
  logo: '/logo.png',
  head: [
    ['link', { rel: 'shortcut icon', href: '/favicon.png' }],
    ['meta', { name: 'keywords', content: 'Storefront, vuejs, nuxtjs, app documentation' }],
    ['meta', { name: 'author', content: 'dvoid-team' }],
  ],
  dest: 'public',
  themeConfig: {
    navbar: false,
    sidebar: [
      ['/', 'Home'],
      ['/install', 'Installation'],
      ['/guide', 'Guide'],
      ['/config', 'Config'],
      ['/deploy', 'Deployment'],
      ['/history', 'Updates history']
    ],
    sidebarDepth: 2
  }
}
