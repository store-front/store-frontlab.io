---
home: true
heroImage: /hero.jpg
description: Step by step guide to install, config and run the Storefront
actionText: Get Started →
actionLink: /install/
features:
- title: Performant
  details: Storefront uses SSR (Server Side Rendering) to bring you the fastest experience with an optimized configuration and lightweight bundle.
- title: Progressive web app
  details: Also accessible in offline mode, and even more performant with caching enabled.
- title: Customizable
  details: With component base architecture and through the available configurations, UI and behavior can be customized to meet all ecommerce needs.
footer: Dvoid. 2019 All rights reserved
---
