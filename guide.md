# Guide

## Client site
Description and features on client site

### Home page
Home page contains
- Main slider
- Category list for navigation
- Products swiper
- Brand slider
- Banners for promoting the store

### Shop page
Shop page is the main page of the store where users can navigate through pages of products.
This page is consisted of 
- Banner
- Sidebar widgets (for filtering products by features)
- Featured products swiper
- Products container (where the actual result is displayed)

Some of the built-in product attributes (visible in the sidebar filters) are:
- Product type (category)
- Size
- Color
- Brand
- Price

### Product detail page
In product detail page you will see
- A breadcrumb with link to product category for easier navigation
- Product image
- Product image swiper (if it has more than one picture)
- Title - price - product info
- Add to cart button
- Favorite button
- Description tab (contains additional information about the product)
- Detail tab (contains product attributes like colors available and sizes etc.)

### Checkout flow


## Admin dashboard
Description and features on admin dashboard

### Home page
Admin home page is currenty unavailable and will be added in the next release

### Orders page
From this page you can see and manage all the orders submitted to your store. By clicking on an order you'll be redirected to the order's detail page where you can see its items, addresses, client, payment status etc.
After any process in gathering product items or issues with the order you have to update the order from its detail page. After updates customer will be notified about the order status by email.

### Products page
All the products added to your store are available in this page. By clicking on an item you'll be redirected to the product's detail page. In this page you're able to update all the product details such as title, images, description, price, categories, tags, inventory status, shipping information and product attributes.

### Users page
From users page you're able to see a list of all users and customers, or you can add new users. You can edit your info by visiting your own profile.

### Analytics page
In this page the amount of sales and user activities are displayed on a line chart. You can use the available options to filter or configure chart's data to get a better view of your store's status.

### Media library
All the media uploaded to your store are accessible through this page. You can delete existing files by hovering the *more* button and clicking delete or upload a new file by clicking *Add New Media*. File's data including name, type, size and url is shown in the detail page of the file.

### Reviews page
All the submitted product reviews are listed in this page. Reviews wont be displayed on the client site before approval. You can read or edit review's text before approving or you can reject or delete the review. Customer and product info are also linked to the review.

### Menus page
Admin app contains a built-in menu builder so you can manage the store's mega menu at ease. Just open the menu page click on a menu, add some store related items (categories, tags, custom links and labels) to the menu and user drag and drop to configure menu structure. From menu items available in the *menu structure* section, click on the configure button to edit its label, url and icon.

### Settings page
All the store's configuration options are listed in this series of pages, divided into many sections such as 
- Site
- General
- Products
- Shipping
- Payments
- Account
- Emails.

Site's settings is consisted of
- site title
- tag line (your store's main description)
- email (admin's email, used for email notifications)
- membership (if enabled users will be able to register to your store, otherwise the only way to create an account is from the admin dashboard)
- date format which configures how the dates must be formatted.


In the general settings page you can edit 
- store's base address
- your selling locations
- your shipping locations 
- your currency.


From the products settings page you can 
- manage weight unit
- manage dimensions unit
- toggle enable reviews
- toggle display verified on product reviews (if enabled this will display a verfied tag on reviews which its author has bought the product)
- toggle only verified reviews (if enabled, only customers who already bought the product will be able to submit reviews)


From the shipping settings page you can add or manage shipping zones.\
A shipping zone is a defined region which will be used for calculation shipment costs. A shipping zone is consisted of regions and shipping methods. there are three shipping methods available at the moment: flat rate (defines a flat rate shipping cost), free shipping and local pickup. You can use this options to configure shipment prices.


Payment methods can be configured from the payments settings page. By clicking on *manage* you can set the required info for the payment method to work.
To be able to setup Stripe payment you have to have an Stripe account first. Create an account from [here](https://dashboard.stripe.com/register) and then configure your stripe payment from the payment methods section in the admin dashboard. 

In the accounts settings page you can manage: guest checkout (if enabled, customers can place order without creating an account, otherwise they're forced to register before submitting an order), enable login during checkout (if enabled, a login link is displayed in the checkout page), enable register during checkout (if enabled, a register link is displayed in the checkout page)


In the emails settings page you can see a list of available emails and mail providers which you can manage. Emails are used in email notifications. At the moment the only available email provider is Mailgun. Click on the *Configure* button to edit its configuration and providing your own config. If you dont have a Mailgun account you can [visit](https://signup.mailgun.com/) to create an account.
