# Config

## Client config
Most of the configuration of the client app and admin dashboard are done through the admin dashboard's settings section.
You only need to set the api address used for production mode.
Or in some cases you may want to configure app's running port etc.
Use the following environment variables to set these options
```
  API_ADDRESS=YOUR_API_ADDRESS
  HOST=0.0.0.0
  PORT=3000
```
## Server config
All the server's configurations are located under the config folder (separated by environments)
The default config is set to use MongoDB on localhost:27017 with database name storefront.
Please note for production use must set a proxy address (Nginx) in `/config/environments/production/server.json` as the following:
```
"proxy": {
  "enabled": true,
  "host": "yourdomain.com/api",
  "ssl": true
}
```
