# Installation

## Requirement
[Node.js](https://nodejs.org)\
[Npm](https://www.npmjs.com/get-npm)\
[MongoDB](https://docs.mongodb.com/manual/installation)

## Installation
### Extract files
Extract the downloaded zip and navigate inside the unziped folder
### Start MongoDB daemon

### Install dependencies
```
  cd Client/Admin
  npm i
```
```
  cd Client/Store
  npm i
```
```
  cd Server
  npm i
```

## Build
### Build client site
In order to build client site run the following commands
```
  cd Client/Store
  npm run build
```

### Build admin dashboard
To increase performance and your server load admin dashboard is preferred to be statically generated and served with Nginx (reverse proxying web serving) instead of built.\
You can refer to [Nginx](/deploy.html#setup-nginx) configuration section for further details.
```
  cd Client/Admin
  npm run build -- --spa
```

Or if you want to build the admin dashboard like the client site you have to change the serving port (default 3000) first to prevent confliction with client site.\
Please refer to [Admin dashboard configuration section](/config.html#client-config).\
then run the following commands
```
  cd Client/Admin
  npm run build
```
## Development
For development simply do the following
```
  cd Client/Admin
  npm run dev
```
```
  cd Client/Store
  npm run dev
```
```
  cd Server
  npm run dev
```
